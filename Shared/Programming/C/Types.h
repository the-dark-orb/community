#pragma once


typedef signed char TSignedInteger8;
typedef signed short int TSignedInteger16;
typedef signed long int TSignedInteger32;
typedef signed long long int TSignedInteger64;

typedef unsigned char TUnsignedInteger8;
typedef unsigned short int TUnsignedInteger16;
typedef unsigned long int TUnsignedInteger32;
typedef unsigned long long int TUnsignedInteger64;


typedef float TDecimal32;
typedef double TDecimal64;
typedef long double TDecimal128;


#if defined(__USE_MODERN_WIDECHAR_TYPE)

typedef wchar_t TCharacter;

#elif defined(__USE_INDEPENDENT_WIDECHAR_TYPE)

typedef TUnsignedInteger16 TCharacter;

#else

#error [Shared-Programming-C-Types.h] : Need enable type of wide characters

#endif

typedef TCharacter *TString;


#if defined(__USE_CLASSIC_CHAR_TYPE)

typedef char TNativeCharacter;

#elif defined(__USE_INDEPENDENT_CHAR_TYPE)

typedef TUnsignedInteger8 TNativeCharacter;

#else

#error [Shared-Programming-C-Types.h] : Need enable type of native characters

#endif

typedef TNativeCharacter *TNativeString;


typedef enum
{
  True = 1 , False = 0
} TBoolean;


typedef void *TPointer;


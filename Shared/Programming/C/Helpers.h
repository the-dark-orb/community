#pragma once


// Indicators

/*

Use only for functions and procedures which must have no any arguments.

*/
#define NO_ARGUMENTS void

/*

An argument that must a local or global variable, not a pointer or any other thing.

*/
#define ARGUMENT(_type) _type

/*

An argument that must a function pointer.

*/
#define FUNCTION_ARGUMENT(_type) ARGUMENT(_type)

/*

An argument that must be a pointer.

*/
#define MEMORY_ARGUMENT(_type) ARGUMENT(_type)

/*

An argument that must be a pointer initially but after expand the macros turned into double-pointer.
Use only for reference purposes.

*/
#define REFERENCED_ARGUMENT(_type) MEMORY_ARGUMENT(_type*)

/*

For long list of declarations.

*/
#define DECLARATIONS(_declarations) __declspec _declarations

// Must be inside __declspec(...) expression
#define DEPRECATED(_message) deprecated(_message)

// Must be inside __declspec(...) expression
#define EXPORT_API dllexport

// Must be inside __declspec(...) expression
#define IMPORT_API dllimport

// Must be inside __declspec(...) expression
#define STRICT_ARGUMENTS noalias

// Must be inside __declspec(...) expression
#define STRICT_RETURN restrict

// Must be inside __declspec(...) expression
#define NEVER_INLINE noinline

// Must be inside __declspec(...) expression
#define NEVER_RETURN noreturn


#define INLINE inline


// Calling conventions

#define CLASSIC_CALLING_CONVENTION __cdecl
#define NATIVE_CALLING_CONVENTION __stdcall
#define REGISTER_CALLING_CONVENTION __vectorcall


// Declarations

#define FUNCTION(_type, _convention, _name, _arguments) _type _convention _name _arguments
#define PROCEDURE(_convention, _name, _arguments) void _convention _name _arguments


// Casting

#define NULL_POINTER ((void*)0)


// Expression builders

#define FOR_BEGIN(_name) _name:
#define FOR_END(_validation, _changing, _name) if _validation { _changing; goto _name; }

